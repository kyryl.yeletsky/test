import './App.css';
import Keyboard from './components/Keyboard/Keyboard';
import TitleScreen from './components/TitleScreen/TitleScreen';
import React from 'react';
import Hangman from './components/Hangman/Hangman';
import words from './assets/hangman_words.json';
import range from './utils/range';
import WordView from './components/WordView/WordView';
import Letter from './components/Letter/Letter';
import Instruction from './components/Instructions/Instructions';

const wordLengths = words.map(word => word.length);
const minLength = Math.min.apply(null, wordLengths);
const maxLength = Math.max.apply(null, wordLengths);

const possibleWordLengths = range(minLength, maxLength + 1);

function getWord(len) {
  const filteredWords  = words.filter(word => word.length === len);
  return filteredWords[Math.floor(Math.random() * filteredWords.length)];
}

function log() {
  return console.log.apply(this, arguments)
}

const TITLE = 0;
const GAME = 1;
const INSTRUCTIONS = 2;

function App() {

  const init = getState();

  const [state, setState] = React.useState(init.initState || {
    selectedLetters: {},
    wordLength: null,
    step: 0,
  });

  const [status, setStatus] = React.useState(init.initStatus || 0);

  const [word, setWord] = React.useState(init.initWord || null);

  const [page, setPage] = React.useState(init.initPage || TITLE);

log(state, word, page)
  const letterSelect = (letter) => {

    const nextStep = state.step + 1;

    if (checkVictory(letter)) {
      win();
    } else if (nextStep >= 11) {
      lose();
    }

    setState(prev => {
      return {
        ...prev,
        selectedLetters: {
          ...prev.selectedLetters,
          [letter]: true
        },
        step: checkValidLetter(letter) ? prev.step : nextStep,
      }
    })
  }

  function win() {
    setStatus(1);
  }

  function lose() {
    setStatus(2);
  } 
 
  function checkValidLetter(letter) {
    return word.indexOf(letter) > -1;
  }

  function checkVictory(letter) {
    const newLetters = {
      ...state.selectedLetters,
      [letter]: true
    };

    return word.split('').every(l => newLetters[l]);
  }

  const wordLengthSelect = (len) => setState(prev => {
    return {
      ...prev,
      wordLength: len
    }
  });

  function start() {
    setWord(getWord(state.wordLength));
    setPage(GAME);
  }

  function restart() {
    setState({
      selectedLetters: {},
      wordLength: null,
      step: 0,
    });

    setStatus(0);

    setWord(null);

    setPage(TITLE);

    clearStorage();
  }

  function clearStorage() {
    localStorage.removeItem('state');
    localStorage.removeItem('status');
    localStorage.removeItem('word');
    localStorage.removeItem('page');
  }

  function saveState() {
    localStorage.setItem('state', JSON.stringify(state));
    localStorage.setItem('status', JSON.stringify(status));
    localStorage.setItem('word', JSON.stringify(word));
    localStorage.setItem('page', JSON.stringify(page));
  }

  function getState() {
   const initState = localStorage.getItem('state');
    const initStatus = localStorage.getItem('status');
    const initWord = localStorage.getItem('word');
    const initPage = localStorage.getItem('page');
    return {
      initState: JSON.parse(initState),
      initStatus: JSON.parse(initStatus),
      initWord: JSON.parse(initWord),
      initPage: JSON.parse(initPage),
    }
  }

  React.useEffect(() => {
    if (page === GAME) {
      saveState();
    }
  });

  let content = null

  let statusContent = '';

  switch (status) {
    case 1:
      statusContent = (
        <h3 className='win'>
          You've won!
        </h3>
      )
      break;
    case 2:
      statusContent= (
        <h3 className='lose'>
          You've lost!
        </h3>
      )
    default:
      break;
  }

  switch (page) {
    case GAME:
      content = (
        <div className='game'>
          <Hangman step={state.step} />
          {
            status ? (
              statusContent
            ) : (
              <p className='text'>It's a {state.wordLength} letter word</p>
            )
          }
          <WordView word={word} selectedLetters={state.selectedLetters} />
          <p className='text'>Play with a word</p>
          <Keyboard disabledLetters={state.selectedLetters} onLetterSelect={letterSelect} status={status} />
        </div>
      )
      break;
  
    case INSTRUCTIONS:
      content = (
        <div>
          <Instruction  onClick={() => setPage(word ? GAME : TITLE)}/>
        </div>
      );
      break;

    default:
     content = (
        <TitleScreen
          setSelected={wordLengthSelect}
          possibleWordLengths={possibleWordLengths}
          selected={state.wordLength}
          onStart={start}
        />
      )
      break;
  }

  return (


    <div className="App">
      {content}
      {page !== 2 ? (
        <Letter onClick={() => setPage(INSTRUCTIONS)} className='restart'>
        Instructions
      </Letter>
      ) : null}
      {status ? (
        <Letter onClick={restart} className='restart'>
          Restart
        </Letter>
      ) : null}
    </div>
  );
}

export default App;
