import styles from './Hangman.module.css';

const steps = [
    <path key="1" d="M1,11 h8" />,

    <path key="2" d="M9,11 v-10" />,

    <path key="3" d="M9,1 h-4" />,

    <path key="4" d="M5,1 v2" />,

    <circle key="5" cx="5" cy="4" r="1" />,

    <path key="6" d="M5,5 v3" />,

    <path key="7" d="M5,5 l-2,2" />,

    <path key="8" d="M5,5 l2,2" />,

    <path key="9" d="M5,8 l-2,2" />,

    <path key="10" d="M5,8 l2,2" />,
]

export default function HangMan({ step }) {
    return (
        <div className={styles.hangman}>
            <svg viewBox="0 0 10 12">

                {steps.slice(0, step)}

                </svg>
        </div>
    )
}