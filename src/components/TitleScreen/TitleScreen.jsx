import React from 'react';
import Letter from '../Letter/Letter';
import styles from './TitleScreen.module.css';


export default function TitleScreen({ possibleWordLengths, selected, setSelected, onStart}) {
    function getRandom() {
        return possibleWordLengths[Math.floor(Math.random() * possibleWordLengths.length)]
    }

    return (
        <div className={styles['title-screen']}>
            <h1 className={styles.title}>The Hangman</h1>
            <p className={styles.text}>
                Let's play <strong>Hangman!</strong><br/>
                How many letters do you want in your word?
            </p>
            <LengthSelect selected={selected} setSelected={setSelected} possibleWordLengths={possibleWordLengths} />
            <Letter className={styles.random} onClick={() => setSelected(getRandom())}>
                Random
            </Letter>
            <Letter selected disabled={!selected} className={styles.start} onClick={onStart}>
                Let's play!
            </Letter>
        </div>
    )
}

function LengthSelect({ setSelected, selected, possibleWordLengths}) {
    return (
        <ul className={`${styles['length-select']} length-select`}>
           {possibleWordLengths.map(n => (
               <li key={n}>
                   <Letter selected={selected === n} onClick={() => setSelected(n)}>
                       {n}
                   </Letter>
               </li>
           ))}
        </ul>
    )
}