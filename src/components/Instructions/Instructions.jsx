import Letter from "../Letter/Letter";

export default function Instruction({ onClick }) {
    
    return (
        <div className="instructions">
            <h1>
                The Hangman
            </h1>
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi voluptatum labore libero laudantium temporibus impedit, pariatur possimus est quibusdam molestiae. Fugit numquam voluptates libero enim molestiae voluptatum esse asperiores quis!
            </p>
            <Letter onClick={onClick} selected className={'got-it'}>
                Got it!
            </Letter>
        </div>
    )
}