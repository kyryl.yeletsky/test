import styles from './Keyboard.module.css';
import Letter from '../Letter/Letter';
const abc = 'abcdefghijklmnopqrstuvwxyz'.split('');

export default function Keyboard({ disabledLetters, onLetterSelect, status}) {

    

    return (
        <div className={`${styles.keyboard} keyboard`}>
            <ul className={`${styles.list} list`}>
                {abc.map(letter => (
                    <li key={letter}>
                        <Letter onClick={
                            status ? () => {} : () => onLetterSelect(letter)
                            }
                            disabled={disabledLetters[letter]}
                        >
                            {letter}
                        </Letter>
                    </li>
                ))}
            </ul>
        </div>
    )
}