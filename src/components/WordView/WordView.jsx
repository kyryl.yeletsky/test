import Letter from "../Letter/Letter";

import styles from './WordView.module.css';

export default function WordView({ word, selectedLetters}) {
    return (
        <ul className={styles['word-view']}>
            {word.split('').map((letter, i) => (
            <li key={letter + i}>
                <Letter className={styles['view-letter']} >
                    {selectedLetters[letter] ? letter : ''}
                </Letter>
            </li>
        ))}
        </ul>
    )
}