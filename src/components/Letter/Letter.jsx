import styles from './Letter.module.css';

export default function Letter({ onClick, disabled, children, selected, className, ...rest}) {
    return (
        <button onClick={onClick} disabled={disabled} className={`letter ${styles.letter} ${className || ''} ${selected ? styles.selected : ''}`} {...rest}>
            {children}
        </button>
    )
}